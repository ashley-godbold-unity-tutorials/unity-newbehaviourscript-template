The file [81-C__Script-NewBehaviourScript.cs.txt](81-C__Script-NewBehaviourScript.cs.txt) will alter the way a new script initializes in Unity.


This is a simple file that does the following:
1. Removes the following warning in Unity when working on PC and using Visual Studio:
    > There are inconsistent line endings in the 'ScriptName.cs' script. Some are Mac OS X (UNIX) and some are Windows.
    > This might lead to incorrect line numbers in stacktraces and compiler errors. Unitron and other text editors can fix this using Convert Line Endings menu commands. 
2. Makes curly braces of the class, Start() function, and Update() function open on the function line rather than a new line.
3. Removes the blank line after the class.
4. Removes the definitions of Start() and Update() as comments in all new scripts.

![changes](images/Change.png)

To use it simply place the [81-C__Script-NewBehaviourScript.cs.txt](81-C__Script-NewBehaviourScript.cs.txt) file in the following folder, making sure to override the one that is already there:

![location](images/Location.PNG)

